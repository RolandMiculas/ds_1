package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.DoctorRepository;

import ro.tuc.ds2020.errorhandler.DuplicateEntryException;
import ro.tuc.ds2020.errorhandler.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import java.util.Optional;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

    private DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorDTO getDoctorById(Integer id) {
        Optional<Doctor> doctor  = doctorRepository.findById(id);
        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }
        return DoctorBuilder.generateDTOFromEntity(doctor.get());
    }

    public Integer insertDoctor(DoctorDTO doctorDTO) {
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());
        if(doctor.isPresent()) {
            throw new DuplicateEntryException("Doctor", "id", doctorDTO.getId().toString());
        }
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }
}
